#!/usr/bin/env groovy
def call(body) {
 def config           = [:]
 body.resolveStrategy = Closure.DELEGATE_FIRST
 body.delegate        = config
 body()

  // Check if repo exists and if not Clone Terraform repo into terraform_repo folder
  def terraform_repo_folder = new File("${WORKSPACE}/terraform_repo/")
  if (!terraform_repo_folder.exists()) {
    dir('terraform_repo') {
      git credentialsId: config.terraform_repo_credentials, branch: 'master', url: config.terraform_repo_url
    }
  }

  try {
    
    // Read Chef Role version from chef_role.config file
    String chef_roles = new File("${WORKSPACE}/chef_roles.config").text
    
    // Copy terraform files to terraform repo folder
    String source = "${WORKSPACE}/infra/terraform/ci";
    File srcDir = new File(source);

    String destination = "${WORKSPACE}/terraform_repo/ci/${BRANCH}-${COMMIT_ID}";
    File destDir = new File(destination);

    FileUtils.copyDirectory(srcDir, destDir);

    // Inject Environment Variables in Terraform files
    def original_terraform_file = new File("${WORKSPACE}/terraform_repo/ci/${BRANCH_ID}-${COMMIT_ID}/compute.tf").text

    def binding = config.template_bindings // This is an array of substitutions - should be defined in Jenkinsfile

    def engine = new groovy.text.SimpleTemplateEngine()
    def template = engine.createTemplate(original_terraform_file).make(binding)

    File injected_terraform_file = new File("${WORKSPACE}/terraform_repo/ci/${BRANCH_ID}-${COMMIT_ID}/compute.tf")
    injected_terraform_file.write template.toString()

    // Commit new terraform files
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: config.terraform_repo_credentials, usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']]) {
      sh("git add --all")
      sh("git commit -m '${BRANCH_ID}-${COMMIT-ID}'")
      sh('git push ' + config.terraform_repo_url)
    }

    // Get Job ID from remote Jenkins server
    def jenkins_build_id_request = httpRequest ignoreSslErrors: true, acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'GET', url: "${config.remote_jenkins_job_url}/api/json"
    def jenkins_build_id_response = new groovy.json.JsonSlurperClassic().parseText(jenkins_build_id_request.content)
    def jenkins_build_id = jenkins_build_id_response.nextBuildNumber

    // Get Job Status from remote Jenkins server
    def jenkins_build_status = ""
    while(jenkins_build_status == ""){
      sleep(5000)
      def jenkins_build_status_request = httpRequest ignoreSslErrors: true, acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'GET', url: "${config.remote_jenkins_job_url}/${jenkins_build_id}/api/json"
      def jenkins_build_status_response = new groovy.json.JsonSlurperClassic().parseText(jenkins_build_status_request.content)
      jenkins_build_status = jenkins_build_status_response.result
    }

    return jenkins_build_status

  }
  catch (Exception error) {
    print "[ERROR] ${error}"
      throw error
  }

}
