#!/usr/bin/env groovy

def call(body) {
  def config           = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate        = config
  body()

  try {
    def liveness_check_results = [:]
    def endpoints_file = File("${WORKSPACE}/liveness_checks.csv")
    endpoints_file.eachLine { String line ->
      def endpoint_map = line.split(",")
      def liveness_test_request = httpRequest ignoreSslErrors: true, acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: endpoint_map[1], url: config.liveness_check_url + endpoint_map[0]
      if(liveness_test_request.getStatus() != endpoint_map[2]){
        print "[ERROR] ${line} got an invalid response status code of liveness_test_request.getStatus()"
          throw error
      }
    }
    return "Success"
  }
  
  catch (Exception error) {
    print "[ERROR] ${error}"
      throw error
  }

}
