# LBG Azure Public IaaS Pipeline Deployment

In order to utilise on demand IaaS deployment in LBG Azure Public Cloud

### Pre-requisites

 - Your Value Stream has been onboarded to Azure Public Cloud by the PCE/SRE team. <contact details/guide>
 - Your application has an approved IMCRA (Internally Managed Cloud Risk Assessment) for the tenant you want to deploy to i.e. RTL/Live. <details/guide>
 - Terraform to enable your application to be deployed in line with the architecture in your approved IMCRA. These must be stored in your applications GitHub repository in a folder at the root of your repository called infra as per the guidelines **HERE** ADD GUIDELINES
 - Your application must be deployed headlessly using a chef role which has been uploaded to the PCE Chef server. See **HERE** for details ADD GUIDE.
 - Your Value Stream must have a GitHub organisation for infrastructure repositories i.e. Chef Cookbooks/Roles and Terraform. See guide **HERE** ADD GUIDE/LINK DAVE ROUSSELLS GUIDE.
 - You have setup the GitHub organisation job in the Azure Jenkins to create jobs for your infrastructure GitHub Organisation. See guide **HERE** ADD GUIDE/LINK DAVE ROUSSELLS GUIDE.
 - You have included a file called **chef_roles.config** at the root of your applications Github Repository detailing the Chef roles and versions required. File format/layout **HERE**.
 - You have included a file called **liveness_checks.csv** at the root of your applications Github Repository detailing the endpoints that should be called on the Blue leg of the deployment to ensure liveness/readiness before switching Blue to Green.  File format/layout **HERE**.
 - You have added this shared library to your pipeline. Details **HERE**

### Shared Library Actions

#### ciDeployment
ciDeployment deploys for a short lived activity and utilises the terraform templates in your application repositories /infra/terraform/ci folder.

It has 2 sub-actions:
##### Create
This action generates new terraform files based on the templates in the application repositories /infra/terraform/ci folder. It is a one off deployment designed to live only for the life of automated testing and then be destroyed immediately afterwards using the destroy sub-action.

Usage:

```
  ci_deployment_result = ciDeployment {
    
    action = "create"
    terraform_repo_url = // STRING This is the application's corresponding terraform repository in your Value Stream's infrastructure organisation.
    terraform_repo_credentials = // STRING This is the id of the stored Jenkins credentials with permissions to clone and push to the terraform repository.
    template_bindings = // MAP This is a map containing the replacements required in your terraform files.
    remote_jenkins_job_url = // STRING This is the remote Azure Jenkins Url. Made up like this **HERE**
    
	}
```

Result: 
ci_deployment_result will be a string containing the result of the remote Jenkins build executing the terraform. 

**Success/Failure**

 Success does not mean that the IaaS is ready to use, only that the resources have been successfully created in the LBG Azure Public Cloud. You will need to add an input step at the beginning of your next stage or directly after this action to pause the pipeline until the Chef recipe completes the callback to let Jenkins know that the build is complete and your application has been successfully deployed on the IaaS requested.

Example Here:


```
input {
  message "Build Complete?"
  ok "Yes"
  parameters {
      string(name: 'IS_COMPLETE', defaultValue: 'Yes', description: 'Complete?')
  }
}
```

##### Destroy
This sub-action removes the previously created terraform files and executes terraform to ensure that the created resources are successfully destroyed.

Usage:


```
  ci_destroy_result = ciDeployment {
    
    action = "destroy"
    terraform_repo_url = // STRING This is the application's corresponding terraform repository in your Value Stream's infrastructure organisation.
    terraform_repo_credentials = // STRING This is the id of the stored Jenkins credentials with permissions to clone and push to the terraform repository.
    remote_jenkins_job_url = // STRING This is the remote Azure Jenkins Url. Made up like this **HERE**
    
	}
```

Result: 
ci_destroy_result will be a string containing the result of the remote Jenkins build executing the terraform to destory the ci IaaS. 

**Success/Failure**

No pause is required after this step, if the ci_destroy_result is 'Success' the Azure resources have successfully been destroyed.


#### blueGreenDeployment

blueGreenDeployment deploys for more more persistent environments and handles blue green deployments to ensure no loss of service.

It has 2 sub-actions:
##### Create
This action generates new terraform files based on the templates in the application repositories /infra/terraform/live folder. This sub-action first checks if there is a current green deployment, if not it will do a first time deployment for the environment specified, if it already exists it will create a blue side deployment along side the green deployment to be checked using the livenessReadinessChecks shared library action

Usage:

```
  bluegreen_deployment_result = blueGreenDeployment {
    
    action = "create"
    environment = //STRING name of the environment to create i.e. live, team-integratiobn
    terraform_repo_url = // STRING This is the application's corresponding terraform repository in your Value Stream's infrastructure organisation.
    terraform_repo_credentials = // STRING This is the id of the stored Jenkins credentials with permissions to clone and push to the terraform repository.
    template_bindings = // MAP This is a map containing the replacements required in your terraform files.
    remote_jenkins_job_url = // STRING This is the remote Azure Jenkins Url. Made up like this **HERE**

	}
```

Result: 
ci_deployment_result will be a string containing the result of the remote Jenkins build executing the terraform. 

**Success/Failure**

 Success does not mean that the IaaS is ready to use, only that the resources have been successfully created in the LBG Azure Public Cloud. You will need to add an input step at the beginning of your next stage or directly after this action to pause the pipeline until the Chef recipe completes the callback to let Jenkins know that the build is complete and your application has been successfully deployed on the IaaS requested.

Example Here:


```
input {
  message "Build Complete?"
  ok "Yes"
  parameters {
      string(name: 'IS_COMPLETE', defaultValue: 'Yes', description: 'Complete?')
  }
}
```

##### Destroy


### Sample Pipeline Usage

```
@Library('lbg-azure-iaas-pipeline-library@master') _

// Set variables

terraform_repo_url = 'git@ghe.service.group:repository-name.git'
template_bindings = 

pipeline {
  agent any
  }
  stages {
    stage('Build Activity') {
      steps {
        deleteDir()
        checkout scm
        //Build stuff here and get package url
      }
    }

    stage('CI Deployment') {
      steps {
        script {
          ci_deployment_result = ciDeployment {
            action = "create"
            terraform_repo_url = terraform_repo_url
            terraform_repo_credentials = 'git-user'
            template_bindings = ["config_environment": "STG", "package_url": package_url, "jenkins_build_url": env.BUILD_URL]
            remote_jenkins_job_url = "https://azure-jenkins"
          }
        }
      }
    }

    stage('CI Testing') {
      input {
        message "Build Complete?"
        ok "Yes"
        parameters {
            string(name: 'IS_COMPLETE', defaultValue: 'Yes', description: 'Complete?')
        }
      }
      steps {
        // Steps to test the deployed build
      }
    }

    stage('CI Destroy') {
      steps {
        script {
          ci_destroy_result = ciDeployment {
            action = "destroy"
            terraform_repo_url = terraform_repo_url
            terraform_repo_credentials = 'git-user'
            remote_jenkins_job_url = "https://azure-jenkins"
          }
        }
      }
    }

    stage('Deploy Live') {
      when {
        branch = "master"
      }
      steps {
        script {
          bluegreen_deployment_result = blueGreenDeployment {
            action = "create"
            terraform_repo_url = terraform_repo_url
            terraform_repo_credentials = 'git-user'
            template_bindings = ["config_environment": "LIVE", "package_url": package_url, "jenkins_build_url": env.BUILD_URL]
            remote_jenkins_job_url = "https://azure-jenkins"
            environment = "live"
          }
        }
      }
    }

    stage('Blue Green Liveness Checks') {
      when {
        branch = "master"
      }
      input {
        message "Build Complete?"
        ok "Yes"
        parameters {
            string(name: 'IS_COMPLETE', defaultValue: 'Yes', description: 'Complete?')
        }
      }
      steps {
        script {
          deployment_success_check = livenessReadinessChecks {
            liveness_check_url = bluegreen_deployment_result.url
          }
          if(deployment_success_check.status == "success"){
            if(bluegreen_deployment_result.type == "blue"){
              bluegreen_destroy_result = blueGreenDeployment {
                action = "destroy"
                terraform_repo_url = terraform_repo_url
                terraform_repo_credentials = 'git-user'
                remote_jenkins_job_url = "https://azure-jenkins"
                environment = "live"
              }
            }
          } else {
            println "[ERROR] Deployment did not pass readiness checks"
            throw error
          }
        }
      }
    }

  }
}
```